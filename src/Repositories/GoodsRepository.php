<?php


namespace App\Repositories;


use App\Entities\Good;
use App\ObjectNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class GoodsRepository extends EntityRepository
{
    public function getOrCreate($id) {
        $good = $this->find($id);
        if (empty($good)) {
            $good = new Good();
        }
        return $good;
    }

    public function updateParams(Good $good, array $values) : bool
    {
        $needUpdate = false;
        foreach (['name' => 'setName', 'price' => 'setPrice'] as $param => $method) {
            if (isset($values[$param])) {
                $needUpdate = true;
                $good->{$method}($values[$param]);
            }
        }
        return $needUpdate;
    }

    public function save(Good $good) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getEntityManager();
        $entityManager->persist($good);
        $entityManager->flush();

        if (empty($good->getId())) {
            $entityManager->refresh($good);
        }
    }

    public function fastRemove(int $id) : void
    {
        /** @var Good $good */
        $good = $this->find($id);
        if (empty($good)) {
            throw new ObjectNotFoundException('Cant find id:' . $id);
        }
        $entityManager = $this->getEntityManager();
        $entityManager->remove($good);
        $entityManager->flush();
    }
}