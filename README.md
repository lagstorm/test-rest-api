# Libs
https://github.com/vlucas/phpdotenv
https://symfony.com/doc/current/components/dependency_injection.html
https://symfony.com/doc/current/components/http_foundation.html
https://symfony.com/doc/current/components/routing.html
https://symfony.com/doc/current/components/validator.html

# Dev lib
https://phpunit.de/getting-started/phpunit-8.html


#Run

RUN ```git clone git@gitlab.com:lagstorm/test-rest-api.git ./```

RUN ```cp .env.example .env```

RUN ```docker-compose up -d```

RUN ```composer install```

OPEN ```http://localhost:8093/```

#Commands

* Create 20 goods [GET]
```http://localhost:8093/api/v1/goods/generate```

* Get all goods [GET]
```http://localhost:8093/api/v1/goods```

* Create order [POST]
```http://localhost:8093/api/v1/orders```
Нужно добавить параметр id[] c id товара(Можно несколько)
```curl -X POST -d 'id[]=37&id[]=38' http://localhost:8093/api/v1/orders```

* Process order [PUT]
```http://localhost:8093/api/v1/orders/{id}```
Нужно добавить параметр paid с суммой заказа
```curl -X PUT -d 'paid=55.66' http://localhost:8093/api/v1/orders/999```


# Process

 * [x] Add base libs
 * [x] Configure dependencies
 * [x] Add controllers processing
 * [x] Tests for service
 * [x] Service PaidService
 * [x] Add ORM/DBAL
 * [ ] Tests for goods
 * [x] Add PUT /api/v1/goods
 * [x] Add POST /api/v1/goods
 * [x] Add GET /api/v1/goods
 * [ ] Tests for orders
 * [x] Add POST /api/v1/orders/{id}
 * [x] Add GET /api/v1/orders/
 * [x] Add GET /api/v1/orders/{id}