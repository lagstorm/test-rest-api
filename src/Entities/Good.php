<?php


namespace App\Entities;

use \Symfony\Component\Validator\Constraints as Assert;
use \Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repositories\GoodsRepository")
 * @ORM\Table(name="goods")
 */
class Good extends AbstractEntity
{

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $name;

    /**
     * @Assert\Positive
     * @ORM\Column(type="float")
     */
    protected $price = 0;

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Good
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Good
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
        return $this;
    }
}