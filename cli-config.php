<?php

use App\Core;

try {
    include("vendor/autoload.php");
    $core = new Core('.');


    $em = $core->getContainer()->get('entityManager');
    $platform = $em->getConnection()->getDatabasePlatform();
    $platform->registerDoctrineTypeMapping('enum', 'string');
    return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);
} catch (Exception $exception) {
    echo "Error! Exception: " . $exception->getMessage();
}