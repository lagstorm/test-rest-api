<?php

use App\Core;

try {
    define('ROOT_DIR', realpath(__DIR__ . '/../'));
    include(ROOT_DIR . "/vendor/autoload.php");
    $core = new Core(ROOT_DIR);
    $core->process();
} catch (Exception $exception) {
    echo "Error! Exception: " . $exception->getMessage();
}
