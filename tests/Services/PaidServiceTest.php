<?php

namespace App\Tests\Services;

use App\Entities\Order;
use App\Services\PaidService;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class PaidServiceTest extends \PHPUnit\Framework\TestCase
{
    public function dataProviderTryProcess() : array
    {
        return [
            'ok situation' => [
                'expected' => true,
                'inPrice' => 12.,
                'orderPrice' => 12.,
                'serviceResponse' => 200,
            ],
            'prices is diff' => [
                'expected' => null,
                'inPrice' => 12.,
                'orderPrice' => 13.,
                'serviceResponse' => 200,
            ],
            'service request fail' => [
                'expected' => false,
                'inPrice' => 12.,
                'orderPrice' => 12.,
                'serviceResponse' => 400,
            ],
        ];
    }

    /**
     * @param $expected
     * @param $inPrice
     * @param $orderPrice
     * @param $serviceResponse
     * @throws \Exception
     *
     * @dataProvider dataProviderTryProcess
     */
    public function testTryProcess($expected, $inPrice, $orderPrice, $serviceResponse)
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist','flush'])
            ->getMock();
        $gazzle = $this->getMockBuilder(Client::class)
            ->onlyMethods(['__call'])
            ->getMock();
        $gazzleResponse = $this->getMockBuilder(Response::class)
            ->onlyMethods(['getStatusCode'])
            ->getMock();
        $gazzle->method('__call')->willReturn($gazzleResponse);
        $gazzleResponse->expects($inPrice == $orderPrice ? $this->once() : $this->never())
            ->method('getStatusCode')
            ->willReturn($serviceResponse);
        $order = $this->getMockBuilder(Order::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getTotalPrice', 'setStatusPaid'])
            ->getMock();
        $order->expects($this->once())
            ->method('getTotalPrice')
            ->willReturn($orderPrice);
        $paidService = new PaidService();
        $paidService->setEntityManager($em);
        $paidService->setGazzle($gazzle);
        $em->expects($expected ? $this->once() : $this->never())
            ->method('persist');
        $em->expects($expected ? $this->once() : $this->never())
            ->method('flush');
        $order->expects($expected ? $this->once() : $this->never())
            ->method('setStatusPaid');

        if ($inPrice != $orderPrice) {
            $this->expectException(\Exception::class);
        }
        $result = $paidService->tryProcess($order, $inPrice);
        if (!is_null($expected)) {
            $this->assertEquals($expected, $result);
        }
        //
    }
}
