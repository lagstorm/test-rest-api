<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="orders")
 */
class Order extends AbstractEntity
{
    const PAID = 'paid';
    const NEW = 'new';

    /** @ORM\Column(type="string", columnDefinition="ENUM('new', 'paid')") */
    protected $status = self::NEW;

    /**
     * @ORM\ManyToMany(targetEntity="Good")
     * @ORM\JoinTable(name="goods_orders",
     *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="good_id", referencedColumnName="id")}
     *      )
     */
    protected $goods;

    public function __construct()
    {
        $this->goods = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addGood($good)
    {
        $this->goods->add($good);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoods(): \Doctrine\Common\Collections\Collection
    {
        return $this->goods;
    }

    public function getTotalPrice() : float {
        $total = 0;
        foreach ($this->goods as $good) {
            $total += $good->getPrice();
        }
        return $total;
    }

    public function jsonSerialize()
    {
        $goods = [];
        $total = 0;
        foreach ($this->getGoods() as $good) {
            $goods[] = $good->getId();
            $total += $good->getPrice();
        }

        return [
            'id' => $this->id,
            'status' => $this->status,
            'orders' => $goods,
            'total' => $total,
        ];
    }

    public function setStatusPaid()
    {
        $this->status = self::PAID;
    }
}