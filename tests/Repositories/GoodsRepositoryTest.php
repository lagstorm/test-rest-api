<?php

namespace App\Tests\Repositories;

use App\Entities\Good;
use App\ObjectNotFoundException;
use App\Repositories\GoodsRepository;
use Doctrine\ORM\EntityManager;

class GoodsRepositoryTest extends \PHPUnit\Framework\TestCase
{

    public function dataProviderSave() : array
    {
        return [
            'create new' => [true, null],
            'update' => [false, 12],
        ];
    }

    /**
     * @param $isReload
     * @param $id
     *
     * @dataProvider dataProviderSave
     */
    public function testSave($isReload, $id)
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['persist','flush','refresh'])
            ->getMock();
        $em->expects($this->once())->method('persist');
        $em->expects($this->once())->method('flush');
        $em->expects($isReload ? $this->once() :$this->never())->method('refresh');
        $repo = $this->getMockBuilder(GoodsRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getEntityManager'])
            ->getMock();
        $repo->expects($this->once())->method('getEntityManager')->willReturn($em);

        $good = $this->getMockBuilder(Good::class)->onlyMethods(['getId'])->getMock();
        $good->expects($this->once())->method('getId')->willReturn($id);
        $repo->save($good);
    }

    public function dataProviderFastRemove() :array
    {
        return [
            'entity exists' => [true],
            'entity not exists' => [false],
        ];
    }

    /**
     * @param $isEntityExists
     * @dataProvider dataProviderFastRemove
     */
    public function testFastRemove($isEntityExists)
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['remove', 'flush'])
            ->getMock();
        $em->expects($isEntityExists ? $this->once() : $this->never())->method('remove');
        $em->expects($isEntityExists ? $this->once() : $this->never())->method('flush');
        $repo = $this->getMockBuilder(GoodsRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getEntityManager', 'find'])
            ->getMock();
        $repo->expects($isEntityExists ? $this->once() : $this->never())->method('getEntityManager')->willReturn($em);
        $repo->expects($this->once())->method('find')->willReturn($isEntityExists ? new Good() : null);

        if (!$isEntityExists) {

            $this->expectException(ObjectNotFoundException::class);
        }
        $repo->fastRemove(1);
    }

    public function testGetOrCreate()
    {
        $entity = $this->getMockBuilder(Good::class)->onlyMethods(['getId'])->getMock();
        $entity->method('getId')->willReturn(12);
        $repo = $this->getMockBuilder(GoodsRepository::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getEntityManager', 'find'])
            ->getMock();
        $repo->expects($this->at(0))->method('find')->willReturn($entity);
        $repo->expects($this->at(1))->method('find')->willReturn(null);

        $firstEntity = $repo->getOrCreate(12);
        $secondEntity = $repo->getOrCreate(12);

        $this->assertEquals(12, $firstEntity->getId());
        $this->assertEmpty($secondEntity->getId());
    }

    public function dataProviderUpdateParams() : array
    {
        return [
            'all fields' => [
                'expectUpdate' => true,
                'params' => ['name' => 'qwe', 'price' => 33.],
                'expectsCall' => ['setName', 'setPrice']
            ],
            'all fields and not used' => [
                'expectUpdate' => true,
                'params' => ['name' => 'qwe', 'price' => 33., 'not' => 'exists'],
                'expectsCall' => ['setName', 'setPrice']
            ],
            'one fields' => [
                'expectUpdate' => true,
                'params' => ['name' => 'qwe'],
                'expectsCall' => ['setName']
            ],
            'one fields and one not used' => [
                'expectUpdate' => true,
                'params' => ['name' => 'qwe', 'not' => 'exists'],
                'expectsCall' => ['setName']
            ],
            'no fields and one not used' => [
                'expectUpdate' => false,
                'params' => ['not' => 'exists'],
                'expectsCall' => []
            ],
        ];
    }

    /**
     * @param $expectUpdate
     * @param $params
     * @param $expectsCall
     *
     * @dataProvider dataProviderUpdateParams
     */
    public function testUpdateParams(bool $expectUpdate, array $params, array $expectsCall)
    {
        $em = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $classMD = $this->getMockBuilder(\Doctrine\ORM\Mapping\ClassMetadata::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repo = new GoodsRepository($em, $classMD);
        $good = $this->getMockBuilder(Good::class)
            ->disableOriginalConstructor()
            ->onlyMethods($expectsCall)
            ->getMock();
        if (!empty($expectsCall)) {
            foreach ($expectsCall as $method) {
                $good->expects($this->once())->method($method);
            }
        }

        $needUpdate = $repo->updateParams($good, $params);
        $this->assertEquals($expectUpdate, $needUpdate);
    }
}
