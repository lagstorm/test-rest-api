<?php


namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntity implements \JsonSerializable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @return integer
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        $json = array();
        foreach($this as $key => $value) {
            $json[$key] = $value;
        }
        return $json;
    }
}