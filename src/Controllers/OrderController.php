<?php

namespace App\Controllers;

use App\Entities\Good;
use App\Entities\Order;
use App\Responses\ApiErrorResponse;
use App\Responses\ApiSuccessResponse;
use App\Services\PaidService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    public function create() : JsonResponse
    {
        try {
            $ids = $this->request->request->get('id');
            array_walk($ids, 'intval');
            if (empty($ids)) {
                throw new \Exception("Order must contain at list one good!");
            }
            $goods = $this->getRepository(Good::class)->findBy(['id' => $ids]);
            $order = new Order();
            foreach ($goods as $good) {
                unset($ids[array_search($good->getId(), $ids)]);
                $order->addGood($good);
            }
            if (!empty($uniqIds)) {
                throw new \Exception("Cant make order: goods with id[" . implode(',', $ids) . "] is not defined");
            }
            $em = $this->getEntityManager();
            $em->persist($order);
            $em->flush($order);
            return new ApiSuccessResponse($order);
        } catch (\Exception $exception) {
            return new ApiErrorResponse(400, ['url' => $this->request->getRequestUri()], $exception->getMessage());
        }
    }

    public function process() : JsonResponse
    {
        try {
            $id = $this->getParam('id', true);
            /** @var Order $order */
            $order = $this->getRepository(Order::class)->find($id);
            if (empty($order)) {
                throw new \Exception('Cant find order with id: ' . $id);
            }

            $paidSum = $this->getRequest()->request->get('paid', 0);
            /** @var PaidService $paidService */
            $paidService = $this->container->get('paidService');
            $paidService->tryProcess($order, $paidSum);

            return new ApiSuccessResponse($order);
        }
        catch(\Exception $e) {
            return new ApiErrorResponse(404, [], $e->getMessage());
        }
    }


    public function list() : JsonResponse
    {
        try {
            $orders = $this->getRepository(Order::class)->findAll();
            return new ApiSuccessResponse($orders);
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(404, null, $exception->getMessage());
        }
    }
}