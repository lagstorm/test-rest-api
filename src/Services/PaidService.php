<?php


namespace App\Services;


use App\Entities\Order;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class PaidService
{

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var Client
     */
    private $gazzle;

    public function setEntityManager(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    public function setGazzle( $gazzle) {
        $this->gazzle = $gazzle;
    }

    public function tryProcess(\App\Entities\Order $order, $paidSum)
    {
        $totalPrice = $order->getTotalPrice();
        if ($paidSum != $totalPrice) {
            throw new \Exception("Can't process order: sum is not correct! Expect: " . $totalPrice);
        }

        $serviceResponse = $this->gazzle->get('http://yandex.ru/all');
        if ($serviceResponse->getStatusCode() == 200) {
            $order->setStatusPaid();
            $this->entityManager->persist($order);
            $this->entityManager->flush($order);
            return true;
        }
        return false;
    }

}