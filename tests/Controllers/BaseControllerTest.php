<?php

namespace App\Tests\Controllers;

use App\Controllers\AbstractController;
use App\Controllers\BaseController;
use App\Responses\ApiErrorResponse;
use Symfony\Component\HttpFoundation\Request;

class BaseControllerTest extends \PHPUnit\Framework\TestCase
{

    public function testSetMessageAndIndex()
    {
        $request = $this->getMockBuilder(Request::class)->onlyMethods(['getRequestUri'])->disableOriginalConstructor()->getMock();
        $request->method('getRequestUri')->willReturn('testurl');
        /** @var BaseController $controller */
        $controller = $this->getMockForAbstractClass(BaseController::class,[$request]);
        $controller->setMessage('test');
        $response = $controller->index();
        $this->assertInstanceOf(ApiErrorResponse::class, $response);
        $this->assertEquals(400, $response->getStatusCode());
        $expected = [
            'status'=>'ERROR',
            'data' => ['url' => 'testurl'],
            'message' => 'test'
        ];
        $this->assertEquals(json_encode($expected), $response->getContent());
    }
}
