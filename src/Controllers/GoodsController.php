<?php

namespace App\Controllers;

use App\Entities\Good;
use App\ObjectNotFoundException;
use App\Repositories\GoodsRepository;
use App\Responses\ApiErrorResponse;
use App\Responses\ApiSuccessResponse;
use Dotenv\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GoodsController extends AbstractController
{

    public function multiCreate() : JsonResponse
    {
        /** @var GoodsRepository $goodRepo */
        $goodRepo = $this->getRepository(Good::class);
        $allGoods = $goodRepo->findAll();
        if (count($allGoods) < 20) {
            for($i = count($allGoods); $i <= 20; $i++) {
                $good = new Good();
                $good->setName('good_' . (rand(100,999999)));
                $good->setPrice((rand(10,999) / 10));
                $this->getEntityManager()->persist($good);
            }
            $this->getEntityManager()->flush();
        }
        return new ApiSuccessResponse();
    }

    public function create() : JsonResponse
    {
        try {
            /** @var GoodsRepository $goodRepo */
            $goodRepo = $this->getRepository(Good::class);
            $good = new Good();
            $goodRepo->updateParams($good, $this->request->request->all());
            /** @var ValidatorInterface $validator */
            $validator = $this->container->get('validator');
            if ($validator->validate($good)) {
                throw new ValidationException("Validation exception");
            }
            $goodRepo->save($good);
            return new ApiSuccessResponse($good);
        }
        catch (ObjectNotFoundException $exception) {
            return new ApiErrorResponse(400, null,"Already exists such goods");
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(400, null, $exception->getMessage());
        }
    }

    public function edit() : JsonResponse
    {
        try {
            $id = $this->getParam('id');
            /** @var GoodsRepository $goodRepo */
            $goodRepo = $this->getRepository(Good::class);
            /** @var Good $good */
            $good = $goodRepo->getOrCreate($id);
            $needUpdate = $goodRepo->updateParams($good, $this->request->request->all());
            if ($needUpdate) {
                /** @var ValidatorInterface $validator */
                $validator = $this->container->get('validator');
                if ($validator->validate($good)) {
                    throw new ValidationException("Validation exception");
                }
                $goodRepo->save($good);
            }
            return new ApiSuccessResponse($good);
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(400, null, $exception->getMessage());
        }
    }

    public function get() : JsonResponse
    {
        try {
            $id = $this->getParam('id', true);
            /** @var Good $good */
            $good = $this->getRepository(Good::class)->find($id);
            if (empty($good)) {
                throw new ObjectNotFoundException('Cant find id:' . $id);
            }
            return new ApiSuccessResponse($good);
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(404, null, $exception->getMessage());
        }
    }

    public function delete() : JsonResponse
    {
        try {
            $id = $this->getParam('id', true);
            $this->getRepository(Good::class)->fastRemove($id);
            return new ApiSuccessResponse();
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(404, null, $exception->getMessage());
        }
    }


    public function list() : JsonResponse
    {
        try {
            $good = $this->getRepository(Good::class)->findAll();
            return new ApiSuccessResponse($good);
        }
        catch (\Exception $exception) {
            return new ApiErrorResponse(404, null, $exception->getMessage());
        }
    }
}