<?php


namespace App\Responses;


use Symfony\Component\HttpFoundation\JsonResponse;

class ApiSuccessResponse extends JsonResponse
{
    public function __construct($data = null, int $status = 200, array $headers = [], bool $json = false)
    {
        $data = [
            'status' => 'SUCCESS',
            'data' => $data,
        ];
        parent::__construct($data, $status, $headers, $json);
    }
}