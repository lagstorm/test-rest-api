<?php

namespace App\Controllers;

use App\Responses\ApiErrorResponse;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{

    /**
     * @var string
     */
    private $message;

    public function index()
    {
        return new ApiErrorResponse(400, ['url' => $this->request->getRequestUri()], $this->message);
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }
}