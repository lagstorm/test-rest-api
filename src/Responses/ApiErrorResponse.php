<?php


namespace App\Responses;


use Symfony\Component\HttpFoundation\JsonResponse;

class ApiErrorResponse extends JsonResponse
{
    public function __construct($code = 400, $data = null, string $message = null)
    {
        $response = [
            'status' => 'ERROR',
        ];
        if (!is_null($data)) {
            $response['data'] = $data;
        }
        if (!is_null($message)) {
            $response['message'] = $message;
        }
        parent::__construct($response, 400, [], false);
    }
}