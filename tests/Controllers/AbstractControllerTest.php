<?php

namespace App\Tests\Controllers;

use App\Controllers\AbstractController;
use Doctrine\Entity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class AbstractControllerTest extends \PHPUnit\Framework\TestCase
{

    public function test__construct()
    {
        $request = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $controller = $this->getMockForAbstractClass(AbstractController::class,[$request]);
        $this->assertSame($request, $controller->getRequest());
        return $controller;
    }

    public function testSetAndGetContainer()
    {
        $request = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        /** @var AbstractController|MockObject $controller */
        $controller = $this->getMockForAbstractClass(AbstractController::class,[$request]);
        /** @var Container|MockObject $container */
        $container = $this->getMockBuilder(Container::class)->disableOriginalConstructor()->getMock();
        $controller->setContainer($container);
        $this->assertSame($container, $controller->getContainer());
        return $controller;
    }

    public function testGetEntityManager()
    {

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        /** @var Container|MockObject $container */
        $container = $this->getMockBuilder(Container::class)
            ->onlyMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();
        $container->expects($this->once())
            ->method('get')
            ->with('entityManager', ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
            ->willReturn($entityManager);
        /** @var AbstractController|MockObject $controller */
        $controller = $this->getMockForAbstractClass(
            AbstractController::class,
            [],
            '',
            false
        );
        $controller->setContainer($container);

        $this->assertEquals($entityManager, $controller->getEntityManager());
    }

    public function testGetRepository()
    {
        /** @var AbstractController|MockObject $controller */
        $controller = $this->getMockForAbstractClass(
            AbstractController::class,
            [],
            '',
            false,
            false,
            true,
            ['getEntityManager']
        );

        $entityManager = $this->getMockBuilder(EntityManager::class)->disableOriginalConstructor()->getMock();
        $controller->expects($this->once())->method('getEntityManager')->willReturn($entityManager);
        $controller->method('getEntityManager')->willReturn($entityManager);

        $repo = $this->getMockBuilder(EntityRepository::class)->disableOriginalConstructor()->getMock();
        $entityManager->method('getRepository')->with(Entity::class)->willReturn($repo);

        $this->assertEquals($repo, $controller->getRepository(Entity::class));
    }

    public function testSetAndGetParams()
    {
        /** @var AbstractController|MockObject $controller */
        $controller = $this->getMockForAbstractClass(AbstractController::class, [], '',false);
        $testData = ['qwe' => 1];
        $controller->setParams($testData);
        $this->assertEquals(1, $controller->getParam('qwe'));
    }
}
