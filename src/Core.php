<?php
namespace App;

use App\Controllers\AbstractController;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Dotenv\Dotenv;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Route;

class Core
{
    /**
     * @var AbstractController
     */
    private $controller;

    /**
     * @var string
     */
    private $method = 'index';
    /**
     * @var ContainerBuilder
     */
    private $containerBuilder;

    public function __construct($rootDir)
    {
        \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');

        $dotenv = Dotenv::createImmutable($rootDir);
        $this->checkConfigVars($dotenv);

        $this->buildContainer();
    }

    protected function checkConfigVars(Dotenv $dotenv)
    {
        $dotenv->load();

        $dotenv->required('ROOT_DIR')->notEmpty();
        $dotenv->required('CODE_DIR')->notEmpty();
        $dotenv->required('CONFIG_DIR')->notEmpty();
        $dotenv->required('PROXY_DIR')->notEmpty();

        $dotenv->required('DATABASE')->notEmpty();
    }

    /**
     * @throws Exception
     */
    public function process()
    {
        try {
            $this->prepareController();
            /** @var Response $response */
            $response = $this->controller->{$this->method}();
        } catch (\Exception $e) {
            $response = new Response($e->getMessage(), 502);
        }
        if (null == $response) {
            $response = new Response(null, 404);
        }
        if ($response instanceof Response) {
            $response->send();
        }
        else {
            throw new \Exception('Expected response is instance of Symfony\Component\HttpFoundation\Response');
        }
    }

    private function buildContainer()
    {

        $this->containerBuilder = new ContainerBuilder();
        $this->containerBuilder->setParameter('rootDir', getenv('ROOT_DIR'));
        $this->containerBuilder->setParameter('connectionUrl', getenv('DATABASE'));
        $this->containerBuilder->setParameter('codeDir', getenv('CODE_DIR'));
        $this->containerBuilder->setParameter('isDebug', true);
        $this->containerBuilder->setParameter('proxyDir', getenv('PROXY_DIR'));

        $loader = new YamlFileLoader($this->containerBuilder, new FileLocator(getenv('CONFIG_DIR')));
        $loader->load('components.yml');
        $this->containerBuilder->compile();
    }

    public function prepareController()
    {
        /** @var UrlMatcher $matcher */
        $matcher = $this->containerBuilder->get('urlMatcher');
        try {
            $request = $this->containerBuilder->get(Request::class);
            /** @var Route $controller */
            $config = $matcher->match($request->getPathInfo());
            list($classname, $this->method) = explode('::', $config['_controller']);
            if (!class_exists($classname)) {
                throw new \Exception('Class not exists! Classname: ' . $classname);
            }
            $this->controller = new $classname($request);
            if (!method_exists($this->controller, $this->method)) {
                throw new \Exception("Method {$this->method} not exists in class {$classname}");
            }
            $this->controller->setContainer($this->containerBuilder);
            $this->controller->setParams($config);
        } catch (Exception $e) {
            /** @var Route $controller */
            $this->controller = $this->containerBuilder->get('\App\Controllers\BaseController');
            $this->controller->setMessage("Can't find route for this url!");
        }
    }

    public function getContainer() : ContainerInterface
    {
        return $this->containerBuilder;
    }
}