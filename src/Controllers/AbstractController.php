<?php

namespace App\Controllers;

use App\Entities\Good;
use App\Repositories\GoodsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AbstractController implements ContainerAwareInterface
{
    /**
     * @var Request
     */
    protected $request;

    protected $params = [];

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Sets the container.
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null) : void
    {
        $this->container = $container;
    }

    /**
     * @param $name
     * @param bool $throwexception
     * @param null $default
     * @return mixed
     * @throws \Exception
     */
    public function getParam($name, $throwexception = false, $default = null)
    {
        if ($throwexception && !isset($this->params[$name])) {
            throw new \Exception("Param {$name} is not set");
        }
        return $this->params[$name] ?? $default;
    }

    public function getEntityManager() : EntityManager
    {
        return $this->container->get('entityManager');
    }

    public function getRepository($class) : EntityRepository
    {
        return $this->getEntityManager()->getRepository($class);
    }

    /**
     * @param array $params
     */
    public function setParams(array $params) : void
    {
        $this->params = $params;
    }

    public function getRequest() : Request
    {
        return $this->request;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}